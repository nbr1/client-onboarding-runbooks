Runbook for Direct Link Set-up.
===============================

PREP NOTES
==========

VLAN 311: LON1 - EUNetworks -> 10.50.1.0/24
--------------
10.101.201.24/29

Hoptroff A:		.25
Hoptroff B:		.26
Tyler endpt:	.28

VLAN 312: LD5 - Equinix -> 10.50.2.0/24
-------------
10.101.201.32/29

Hoptroff A:		.33
Hoptroff B:		.34
Tyler endpt:	.36

!
1. == SWITCH CONFIG ==

1.1 Configure the switches. For Tyler, we had 2 cables to the rack: one fibre and 
one copper. The copper went to 'sw02 access' and the fibre went to 'sw02 core';
using both as examples.

! == sw02 access == !
interface GigabitEthernet1/0/18
 description Tyler Connection from LD5 (Eqinux in Slough) via Colt
 switchport access vlan 312
!

! == sw02 core == !
interface GigabitEthernet0/14
 description Tyler Connection from LON1 (Interaction in Brick Lane) via EUNetworks
 switchport access vlan 311
!

2. == FIREWALL CONFIG ==

2.1 The vlans configured at the switches above are configured as vlans in the firewall
to provide a match with the interfaces, as follows.

interface Ethernet0/0.311
 description Tyler Connection from LON1 (Interaction in Brick Lane) via EUNetworks
 vlan 311
 nameif TYLER-LON1
 security-level 50
 ip address 10.101.201.25 255.255.255.248 standby 10.101.201.26 
!
interface Ethernet0/0.312
 description Tyler Connection from LD5 (Eqinux in Slough) via Colt
 vlan 312
 nameif TYLER-LD5
 security-level 50
 ip address 10.101.201.33 255.255.255.248 standby 10.101.201.34 
!

2.2 Now configure the object networks to refer to them in the firewall configuration.

! 
object network TYLER-LON1-DIRECT-LAN1
 subnet 10.50.1.0 255.255.255.0
object network TYLER-LD5-DIRECT-LAN1
 subnet 10.50.2.0 255.255.255.0
!

2.3 Set up the access-lists.

2.3.1 Compliance server access:

access-list OUTSIDE_access_in extended permit tcp any host 10.101.30.141 eq 55239

NOTE:
Should the above line be deleted since we now have a much refined access-list for 55239 below?

2.3.2 Time, Ping, SNMP and Compliance server access:

access-list TYLER-LON1_access_in extended permit tcp object TYLER-LON1-DIRECT-LAN1 host 10.101.100.141 eq 55239
access-list TYLER-LON1_access_in extended permit udp object TYLER-LON1-DIRECT-LAN1 object TIME-DISTRIBUTION-LAN range 319 320
access-list TYLER-LON1_access_in extended permit icmp object TYLER-LON1-DIRECT-LAN1 object TIME-DISTRIBUTION-LAN
access-list TYLER-LON1_access_in extended permit udp object TYLER-LON1-DIRECT-LAN1 object INSIDE-VIRTUAL-NAT range 611 612
access-list TYLER-LD5_access_in extended permit tcp object TYLER-LD5-DIRECT-LAN1 host 10.101.100.141 eq 55239
access-list TYLER-LD5_access_in extended permit udp object TYLER-LD5-DIRECT-LAN1 object TIME-DISTRIBUTION-LAN range 319 320
access-list TYLER-LD5_access_in extended permit icmp object TYLER-LD5-DIRECT-LAN1 object TIME-DISTRIBUTION-LAN
access-list TYLER-LD5_access_in extended permit udp object TYLER-LD5-DIRECT-LAN1 object INSIDE-VIRTUAL-NAT range 611 612
!

NOTE:
Should SNMP be restricted to NAGIOS-HOST? 
So, instead of:

access-list TYLER-LD5_access_in extended permit udp object TYLER-LD5-DIRECT-LAN1 object INSIDE-VIRTUAL-NAT range 611 612

We have:

access-list TYLER-LD5_access_in extended permit udp object TYLER-LD5-DIRECT-LAN1 host NAGIOS-HOST range 611 612

2.3.3 Specify the mtu metric.

mtu TYLER-LON1 1500
mtu TYLER-LD5 1500

2.3.4 Apply the access-lists with access-groups bound to the interfaces.

access-group TYLER-LON1_access_in in interface TYLER-LON1
access-group TYLER-LD5_access_in in interface TYLER-LD5

2.3.5 Add routing.

route TYLER-LON1 10.50.1.0 255.255.255.0 10.101.201.28 1
route TYLER-LD5 10.50.2.0 255.255.255.0 10.101.201.36 1

3. Add routes on all Clocks.

[root@gmc01 ~]# route add -net 10.50.2.0/24 gw 10.101.100.1

Then:-

[root@gmc01 ~]# route
Kernel IP routing table
Destination     Gateway         Genmask         Flags Metric Ref    Use Iface
default         gateway         0.0.0.0         UG    100    0        0 eno1
10.0.53.0       10.101.100.1    255.255.255.0   UG    0      0        0 enp1s0
10.1.53.0       10.101.100.1    255.255.255.0   UG    0      0        0 enp1s0
10.50.1.0       10.101.100.1    255.255.255.0   UG    0      0        0 enp1s0
10.50.2.0       10.101.100.1    255.255.255.0   UG    0      0        0 enp1s0
10.64.132.0     10.101.100.1    255.255.255.0   UG    0      0        0 enp1s0
10.67.95.0      10.101.100.1    255.255.255.0   UG    0      0        0 enp1s0
10.67.96.0      10.101.100.1    255.255.255.0   UG    0      0        0 enp1s0
10.69.131.0     10.101.100.1    255.255.255.0   UG    0      0        0 enp1s0
10.72.4.0       10.101.100.1    255.255.255.0   UG    0      0        0 enp1s0
10.72.8.0       10.101.100.1    255.255.255.0   UG    0      0        0 enp1s0
10.72.65.0      10.101.100.1    255.255.255.0   UG    0      0        0 enp1s0
10.89.132.0     0.0.0.0         255.255.255.128 U     100    0        0 eno2
10.89.132.0     10.89.132.1     255.255.254.0   UG    0      0        0 eno2
10.101.20.0     0.0.0.0         255.255.255.0   U     100    0        0 eno1
10.101.100.0    0.0.0.0         255.255.255.0   U     100    0        0 enp1s0
10.177.84.0     10.101.100.1    255.255.255.0   UG    0      0        0 enp1s0
49.14.115.0     10.101.100.1    255.255.255.0   UG    0      0        0 enp1s0
142.205.41.128  10.101.100.1    255.255.255.128 UG    0      0        0 enp1s0
172.17.111.0    10.101.100.1    255.255.255.224 UG    0      0        0 enp1s0
172.17.113.0    10.101.100.1    255.255.255.224 UG    0      0        0 enp1s0
192.168.99.0    10.101.100.1    255.255.255.0   UG    0      0        0 enp1s0
192.168.237.0   10.101.100.1    255.255.255.0   UG    0      0        0 enp1s0
[root@gmc01 ~]#

NOTE: gmc01 used as an example; this needs to be done on all clocks.

==== FINIS ====



